@extends('layouts.app')

@section('title', 'List Transport')

@push('style')
    <!-- CSS Libraries -->
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Transport</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-transport active"><a href="#">List Transport</a></div>
                    <div class="breadcrumb-transport"><a href="#"></a></div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Tabel Transport</h4>
                                <div class="card-header-action d-flex">
                                    <form action="{{ route('transports.index') }}" method="GET">
                                        <div class="input-group pr-2">
                                            <input type="text" class="form-control" name="search" placeholder="Search"
                                                value="{{ request()->query('search') }}">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    <a href="{{ route('transports.create') }}">
                                        <button class="btn btn-primary">
                                            Add Transport
                                        </button>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-bordered table-md table">
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Photo</th>
                                            <th>Number Phone</th>
                                            <th>Address</th>
                                            <th>Price</th>
                                            <th>Time</th>
                                            <th>Capacity</th>
                                            <th>City</th>
                                            <th>Owner</th>
                                            <th>Type</th>
                                            <th>Description</th>
                                            <th class="text-center">Action</th>
                                        </tr>

                                        @if ($transports->isEmpty())
                                            <tr>
                                                <td colspan="8" class="text-center">No data available</td>
                                            </tr>
                                        @endif
                                        @foreach ($transports as $transport)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $transport->name }}</td>
                                                <td><img width="100" height="100"
                                                        src="{{ asset("storage/transports/$transport->photo") }}"
                                                        alt="">
                                                </td>
                                                <td>{{ $transport->number_phone }}</td>
                                                <td>{{ $transport->address }}</td>
                                                <td>{{ $transport->price }}</td>
                                                <td>{{ $transport->time }}</td>
                                                <td>{{ $transport->capacity }}</td>
                                                <td>{{ $transport->city }}</td>
                                                <td>{{ $transport->owner }}</td>
                                                <td>{{ $transport->type }}</td>
                                                <td>{{ $transport->description }}</td>
                                                <td>
                                                    <a href="{{ route('transports.edit', ['transport' => $transport]) }}"
                                                        class="btn btn-warning btn-sm">Edit</a>
                                                    <form action="{{ route('transports.destroy', $transport->id) }}"
                                                        method="POST" class="d-inline">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <nav class="d-inline-block">
                                    {{ $transports->links() }}
                                </nav>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->

    <!-- Page Specific JS File -->
@endpush
