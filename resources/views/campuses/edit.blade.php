@extends('layouts.app')

@section('title', 'Edit Campus')

@push('style')
    <!-- CSS Libraries -->
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Campuses</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Campuses</a></div>
                    <div class="breadcrumb-item"><a href="#">Edit</a></div>
                </div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <form action="{{ route('campuses.update', ['campus' => $campus]) }}" method="POST"
                                enctype="multipart/form-data">
                                @method('PUT')
                                @csrf
                                <div class="card-header">
                                    <h4>Edit Campuses</h4>
                                </div>
                                <div class="card-body row">
                                    <div class="col-md-6 col-lg-6">
                                        {{-- form name --}}
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror "
                                                name="name" value="{{ $campus->name }}" required="">
                                            @error('name')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form address --}}
                                        <div class="form-group">
                                            <label>Address</label>
                                            <input type="text"
                                                class="form-control @error('address') is-invalid @enderror " name="address"
                                                value="{{ $campus->address }}" required="">
                                            @error('address')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form number_phone --}}
                                        <div class="form-group">
                                            <label>Number Phone</label>
                                            <input type="text"
                                                class="form-control @error('number_phone') is-invalid @enderror "
                                                name="number_phone" value="{{ $campus->number_phone }}" required="">
                                            @error('number_phone')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        {{-- form email --}}
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror "
                                                name="email" value="{{ $campus->email }}" required="">
                                            @error('email')
                                                <div class="invalid-feedback">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">

                                        {{-- form description --}}
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control @error('description') is-invalid @enderror " name="description" value=""
                                                required="" style="height: 100px;">{{ $campus->description }}</textarea>
                                            @error('description')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        {{-- form photo cover --}}
                                        <div class="form-group">
                                            <label>Photo</label>
                                            @if ($campus->photo)
                                                <figure class="imagecheck-figure">
                                                    <img src="{{ asset('storage/campuses/' . $campus->photo) }}"
                                                        alt="" class="imagecheck-image">
                                                </figure>
                                            @endif

                                            <input type="file" class="form-control @error('photo') is-invalid @enderror "
                                                name="photo" value="@$campus->photo)">
                                            @error('photo')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        {{-- form instagram --}}
                                        <div class="form-group">
                                            <label>Instagram</label>
                                            <input class="form-control @error('instagram') is-invalid @enderror "
                                                name="instagram" value="{{ $campus->instagram }}" required=""
                                                type="text">
                                            @error('instagram')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        {{-- form website --}}
                                        <div class="form-group">
                                            <label>Website</label>
                                            <input class="form-control @error('website') is-invalid @enderror "
                                                name="website" value="{{ $campus->website }}" required=""
                                                type="text">
                                            @error('website')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->

    <!-- Page Specific JS File -->
@endpush
