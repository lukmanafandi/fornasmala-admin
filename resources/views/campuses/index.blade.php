@extends('layouts.app')

@section('title', 'List Campus')

@push('style')
    <!-- CSS Libraries -->
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Campus</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-campus active"><a href="#">Campus</a></div>
                    <div class="breadcrumb-campus"><a href="#">List</a></div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Tabel Campus</h4>
                                <div class="card-header-action d-flex">
                                    <form action="{{ route('campuses.index') }}" method="GET">
                                        <div class="input-group pr-2">
                                            <input type="text" class="form-control" name="search" placeholder="Search"
                                                value="{{ request()->query('search') }}">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    <a href="{{ route('campuses.create') }}">
                                        <button class="btn btn-primary">
                                            Add Campus
                                        </button>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-bordered table-md table">
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Photo</th>
                                            <th>Number Phone</th>
                                            <th>Address</th>
                                            <th>Instagram</th>
                                            <th>Description</th>
                                            <th class="text-center">Action</th>
                                        </tr>

                                        @if ($campuses->isEmpty())
                                            <tr>
                                                <td colspan="8" class="text-center">No data available</td>
                                            </tr>
                                        @endif
                                        @foreach ($campuses as $campus)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $campus->name }}</td>
                                                <td>{{ $campus->email }}</td>
                                                <td><img width="100" height="100"
                                                        src="{{ asset("storage/campuses/$campus->photo") }}" alt="">
                                                </td>
                                                <td>{{ $campus->number_phone }}</td>
                                                <td>{{ $campus->address }}</td>
                                                <td>{{ $campus->instagram }}</td>
                                                <td>{{ $campus->description }}</td>
                                                <td>
                                                    <a href="{{ route('campuses.edit', ['campus' => $campus]) }}"
                                                        class="btn btn-warning btn-sm">Edit</a>
                                                    <form action="{{ route('campuses.destroy', $campus->id) }}"
                                                        method="POST" class="d-inline">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <nav class="d-inline-block">
                                    {{ $campuses->links() }}
                                </nav>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->

    <!-- Page Specific JS File -->
@endpush
