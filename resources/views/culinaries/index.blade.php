@extends('layouts.app')

@section('title', 'List Culinary')

@push('style')
    <!-- CSS Libraries -->
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Culinary</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-culinary active"><a href="#">Culinary</a></div>
                    <div class="breadcrumb-culinary"><a href="#">List</a></div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Tabel Culinary</h4>
                                <div class="card-header-action d-flex">
                                    <form action="{{ route('culinaries.index') }}" method="GET">
                                        <div class="input-group pr-2">
                                            <input type="text" class="form-control" name="search" placeholder="Search"
                                                value="{{ request()->query('search') }}">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    <a href="{{ route('culinaries.create') }}">
                                        <button class="btn btn-primary">
                                            Add Culinary
                                        </button>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-bordered table-md table">
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Is Recommended</th>
                                            <th>Photo</th>
                                            <th>Number Phone</th>
                                            <th>Address</th>
                                            <th>Price</th>
                                            <th>Rating</th>
                                            <th>Type</th>
                                            <th>Description</th>
                                            <th class="text-center">Action</th>
                                        </tr>

                                        @if ($culinaries->isEmpty())
                                            <tr>
                                                <td colspan="8" class="text-center">No data available</td>
                                            </tr>
                                        @endif
                                        @foreach ($culinaries as $culinary)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $culinary->name }}</td>
                                                <td>{{ $culinary->is_recommended }}</td>
                                                <td><img width="100" height="100"
                                                        src="{{ asset("storage/culinaries/$culinary->photo") }}"
                                                        alt="">
                                                </td>
                                                <td>{{ $culinary->number_phone }}</td>
                                                <td>{{ $culinary->address }}</td>
                                                <td>{{ $culinary->price }}</td>
                                                <td>{{ $culinary->rating }}</td>
                                                <td>{{ $culinary->type }}</td>
                                                <td>{{ $culinary->description }}</td>
                                                <td>
                                                    <a href="{{ route('culinaries.edit', ['culinary' => $culinary]) }}"
                                                        class="btn btn-warning btn-sm">Edit</a>
                                                    <form action="{{ route('culinaries.destroy', $culinary->id) }}"
                                                        method="POST" class="d-inline">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <nav class="d-inline-block">
                                    {{ $culinaries->links() }}
                                </nav>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->

    <!-- Page Specific JS File -->
@endpush
