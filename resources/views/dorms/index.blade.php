@extends('layouts.app')

@section('title', 'List Dorms')

@push('style')
    <!-- CSS Libraries -->
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Dorms</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-dorm active"><a href="#">List Dorms</a></div>
                    <div class="breadcrumb-dorm"><a href="#"></a></div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Tabel Dorms</h4>
                                <div class="card-header-action d-flex">
                                    <form action="{{ route('dorms.index') }}" method="GET">
                                        <div class="input-group pr-2">
                                            <input type="text" class="form-control" name="search" placeholder="Search"
                                                value="{{ request()->query('search') }}">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    <a href="{{ route('dorms.create') }}">
                                        <button class="btn btn-primary">
                                            Add Dorms
                                        </button>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-bordered table-md table">
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Photo</th>
                                            <th>Number Phone</th>
                                            <th>Address</th>
                                            <th>Price</th>
                                            <th>Time</th>
                                            <th>Facilities</th>
                                            <th>City</th>
                                            <th>Owner</th>
                                            <th>Type</th>
                                            <th>Description</th>
                                            <th class="text-center">Action</th>
                                        </tr>

                                        @if ($dorms->isEmpty())
                                            <tr>
                                                <td colspan="8" class="text-center">No data available</td>
                                            </tr>
                                        @endif
                                        @foreach ($dorms as $dorm)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $dorm->name }}</td>
                                                <td><img width="100" height="100"
                                                        src="{{ asset("storage/dorms/$dorm->photo") }}" alt="">
                                                </td>
                                                <td>{{ $dorm->number_phone }}</td>
                                                <td>{{ $dorm->address }}</td>
                                                <td>{{ $dorm->price }}</td>
                                                <td>{{ $dorm->time }}</td>
                                                <td>{{ $dorm->facilities }}</td>
                                                <td>{{ $dorm->city }}</td>
                                                <td>{{ $dorm->owner }}</td>
                                                <td>{{ $dorm->type }}</td>
                                                <td>{{ $dorm->description }}</td>
                                                <td>
                                                    <a href="{{ route('dorms.edit', ['dorm' => $dorm]) }}"
                                                        class="btn btn-warning btn-sm">Edit</a>
                                                    <form action="{{ route('dorms.destroy', $dorm->id) }}" method="POST"
                                                        class="d-inline">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <nav class="d-inline-block">
                                    {{ $dorms->links() }}
                                </nav>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->

    <!-- Page Specific JS File -->
@endpush
