@extends('layouts.app')

@section('title', 'List Organization')

@push('style')
    <!-- CSS Libraries -->
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Organization</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-organization active"><a href="#">Organization</a></div>
                    <div class="breadcrumb-organization"><a href="#">List</a></div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Tabel Organization</h4>
                                <div class="card-header-action d-flex">
                                    <form action="{{ route('organizations.index') }}" method="GET">
                                        <div class="input-group pr-2">
                                            <input type="text" class="form-control" placeholder="Search" name="search"
                                                value="{{ request()->query('search') }}">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                    <a href="{{ route('organizations.create') }}">
                                        <button class="btn btn-primary">
                                            Add Organization
                                        </button>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-bordered table-md table">
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Photo</th>
                                            <th>Number Phone</th>
                                            <th>Address</th>
                                            <th>Instagram</th>
                                            <th>Description</th>
                                            <th class="text-center">Action</th>
                                        </tr>

                                        @if ($organizations->isEmpty())
                                            <tr>
                                                <td colspan="8" class="text-center">No data available</td>
                                            </tr>
                                        @endif
                                        @foreach ($organizations as $organization)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $organization->name }}</td>
                                                <td>{{ $organization->email }}</td>
                                                <td><img width="100" height="100"
                                                        src="{{ asset("storage/organizations/$organization->photo") }}"
                                                        alt="">
                                                </td>
                                                <td>{{ $organization->number_phone }}</td>
                                                <td>{{ $organization->address }}</td>
                                                <td>{{ $organization->instagram }}</td>
                                                <td>{{ $organization->description }}</td>
                                                <td>
                                                    <a href="{{ route('organizations.edit', ['organization' => $organization]) }}"
                                                        class="btn btn-warning btn-sm">Edit</a>
                                                    <form action="{{ route('organizations.destroy', $organization->id) }}"
                                                        method="POST" class="d-inline">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <nav class="d-inline-block">
                                    <ul class="pagination mb-0">
                                        <li class="page-organization disabled">
                                            <a class="page-link" href="#" tabindex="-1"><i
                                                    class="fas fa-chevron-left"></i></a>
                                        </li>
                                        <li class="page-organization active"><a class="page-link" href="#">1 <span
                                                    class="sr-only">(current)</span></a></li>
                                        <li class="page-organization">
                                            <a class="page-link" href="#">2</a>
                                        </li>
                                        <li class="page-organization"><a class="page-link" href="#">3</a></li>
                                        <li class="page-organization">
                                            <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->

    <!-- Page Specific JS File -->
@endpush
