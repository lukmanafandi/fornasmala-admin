<?php

use App\Http\Controllers\API\V1\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\V1\BookController;
use App\Http\Controllers\API\V1\CulinaryController;
use App\Http\Controllers\API\V1\DormController;
use App\Http\Controllers\API\V1\OrganizationController;
use App\Http\Controllers\API\V1\TransportController;
use App\Http\Controllers\API\V1\NewsController;
use App\Http\Controllers\API\V1\CampusController;
use App\Http\Controllers\API\V1\StudentController;
use App\Http\Controllers\API\V1\SummaryController;
use App\Http\Controllers\API\V1\WritingController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// group middleware
//    book
Route::get('books', [BookController::class, 'index']);
Route::get('books/{id}', [BookController::class, 'show']);
Route::post('books', [BookController::class, 'store']);
Route::put('books/{id}', [BookController::class, 'update']);
Route::delete('books/{id}', [BookController::class, 'destroy']);

//    writing
Route::get('writings', [WritingController::class, 'index']);
Route::get('writings/{id}', [WritingController::class, 'show']);
Route::post('writings', [WritingController::class, 'store']);
Route::put('writings/{id}', [WritingController::class, 'update']);
Route::delete('writings/{id}', [WritingController::class, 'destroy']);

//    summary
Route::get('summaries', [SummaryController::class, 'index']);
Route::get('summaries/{id}', [SummaryController::class, 'show']);
Route::post('summaries', [SummaryController::class, 'store']);
Route::put('summaries/{id}', [SummaryController::class, 'update']);
Route::delete('summaries/{id}', [SummaryController::class, 'destroy']);
Route::post('login', [AuthController::class, 'login']);
Route::post('logout', [AuthController::class, 'logout']);


// campuses
Route::get('campuses', [CampusController::class, 'index']);
Route::get('campuses/{id}', [CampusController::class, 'show']);
Route::post('campuses', [CampusController::class, 'store']);
Route::put('campuses/{id}', [CampusController::class, 'update']);
Route::delete('campuses/{id}', [CampusController::class, 'destroy']);

// culinary
Route::get('culinaries', [CulinaryController::class, 'index']);
Route::get('culinaries/{id}', [CulinaryController::class, 'show']);
Route::post('culinaries', [CulinaryController::class, 'store']);
Route::put('culinaries/{id}', [CulinaryController::class, 'update']);
Route::delete('culinaries/{id}', [CulinaryController::class, 'destroy']);

// Dorms
Route::get('dorms', [DormController::class, 'index']);
Route::get('dorms/{id}', [DormController::class, 'show']);
Route::post('dorms', [DormController::class, 'store']);
Route::put('dorms/{id}', [DormController::class, 'update']);
Route::delete('dorms/{id}', [DormController::class, 'destroy']);

// news
Route::get('news', [NewsController::class, 'index']);
Route::get('news/{id}', [NewsController::class, 'show']);
Route::post('news', [NewsController::class, 'store']);
Route::put('news/{id}', [NewsController::class, 'update']);
Route::delete('news/{id}', [NewsController::class, 'destroy']);

// organizations
Route::get('organizations', [OrganizationController::class, 'index']);
Route::get('organizations/{id}', [OrganizationController::class, 'show']);
Route::post('organizations', [OrganizationController::class, 'store']);
Route::put('organizations/{id}', [OrganizationController::class, 'update']);
Route::delete('organizations/{id}', [OrganizationController::class, 'destroy']);

// transports
Route::get('transports', [TransportController::class, 'index']);
Route::get('transports/{id}', [TransportController::class, 'show']);
Route::post('transports', [TransportController::class, 'store']);
Route::put('transports/{id}', [TransportController::class, 'update']);
Route::delete('transports/{id}', [TransportController::class, 'destroy']);
