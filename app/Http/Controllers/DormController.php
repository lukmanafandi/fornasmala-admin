<?php

namespace App\Http\Controllers;

use App\Models\Dorm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dorms = Dorm::latest();

        if ($request->has('search') && $request->search != '') {
            $dorms = $dorms->where('name', 'like', '%' . $request->search . '%')
                ->orWhere('description', 'like', '%' . $request->search . '%')
                ->orWhere('address', 'like', '%' . $request->search . '%')
                ->orWhere('price', 'like', '%' . $request->search . '%');
        }

        $dorms = $dorms->paginate(10);

        return view('dorms.index',  ['dorms' => $dorms, 'type_menu' => 'dorm']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dorms.create', ['type_menu' => 'dorm']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'address' => 'required|max:255',
            'price' => 'required|max:255',
            'time' => 'required|max:255',
            'facilities' => 'required|max:255',
            'city' => 'required|max:255',
            'owner' => 'required|max:255',
            'type' => 'required|max:255',
            'photo' => 'required|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'number_phone' => 'required|max:255',
        ]);

        $dorm = new Dorm();
        $dorm->fill($validateData);
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/dorms/photos', $photo, $fileName);

            $dorm->photo = 'photos/' . $fileName;
        }

        $dorm->save();

        return redirect()->route('dorms.index')->with('success', 'Dorm successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dorm  $dorm
     * @return \Illuminate\Http\Response
     */
    public function show(Dorm $dorm)
    {
        return view('dorms.show', ['dorm' => $dorm, 'type_menu' => 'dorm']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dorm  $dorm
     * @return \Illuminate\Http\Response
     */
    public function edit(Dorm $dorm)
    {
        return view('dorms.edit', ['dorm' => $dorm, 'type_menu' => 'dorm']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dorm  $dorm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dorm $dorm)
    {
        $validateData = $request->validate([
            'name' => 'max:255',
            'description' => 'max:255',
            'address' => 'max:255',
            'price' => 'max:255',
            'time' => 'max:255',
            'facilities' => 'max:255',
            'city' => 'max:255',
            'owner' => 'max:255',
            'type' => 'max:255',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'number_phone' => 'max:255',
        ]);

        $dorm->fill($validateData);
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/dorms/photos', $photo, $fileName);

            $dorm->photo = 'photos/' . $fileName;
        }

        $dorm->save();

        return redirect()->route('dorms.index')->with('success', 'Dorm successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dorm  $dorm
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dorm $dorm)
    {
        $dorm->delete();

        return redirect()->route('dorms.index')->with('success', 'Dorm successfully deleted');
    }
}
