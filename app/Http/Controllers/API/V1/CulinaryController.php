<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Culinary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CulinaryController extends Controller
{
    public function index()
    {
        $culinaries = Culinary::latest()->get();
        $culinaries->map(function ($culinary) {
            $culinary->photo = env('APP_URL') . Storage::url('culinaries/' . $culinary->photo);
            return $culinary;
        });
        return response()->json([
            'success' => true,
            'message' => 'List Data Culinary',
            'data'    => $culinaries
        ], 200);
    }

    public function show($id)
    {
        $culinary = Culinary::find($id);

        $culinary->photo = env('APP_URL') . Storage::url('culinaries/' . $culinary->photo);

        if (!$culinary) {
            return response()->json([
                'success' => false,
                'message' => 'Culinary tidak ditemukan',
                'data'    => null
            ], 404);
        }
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'email' => 'required|max:255',
            'number_phone' => 'required|max:255',
            'website' => 'required|max:255',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $culinary = new Culinary();

        $culinary->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            Storage::putFileAs('public/culinaries', $photo, $filename);
            $culinary->photo = $filename;
        }

        $culinary->save();

        return response()->json([
            'success' => true,
            'message' => 'Post Berhasil Disimpan!',
            'data'    => $culinary
        ], 200);
    }

    public function update(Request $request)
    {
        $culinary = Culinary::find($request->id);

        if (!$culinary) {
            return response()->json([
                'success' => false,
                'message' => 'Post Tidak Ditemukan!',
                'data'    => null
            ], 404);
        }

        $validateData = $request->validate([
            'name' => 'max:255',
            'address' => 'max:255',
            'email' => 'max:255',
            'number_phone' => 'max:255',
            'website' => 'max:255',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $culinary->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            Storage::putFileAs('public/culinaries', $photo, $filename);
            Storage::delete('public/culinaries/' . $culinary->photo);
            $culinary->photo = $filename;
        }

        $culinary->save();

        return response()->json([
            'success' => true,
            'message' => 'Post Berhasil Diupdate!',
            'data'    => $culinary
        ], 200);
    }

    public function destroy($id)
    {
        $culinary = Culinary::find($id);

        if (!$culinary) {
            return response()->json([
                'success' => false,
                'message' => 'Post Tidak Ditemukan!',
                'data'    => null
            ], 404);
        }

        Storage::delete('public/culinaries/' . $culinary->photo);
        $culinary->delete();

        return response()->json([
            'success' => true,
            'message' => 'Post Berhasil Dihapus!',
            'data'    => $culinary
        ], 200);
    }
}
