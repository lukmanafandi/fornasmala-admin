<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class OrganizationController extends Controller
{
    //    protected $fillable = [
    //     'name',
    //     'address',
    //     'number_phone',
    //     'photo',
    //     'email',
    //     'description',
    //     'instagram',
    // ];

    public function index()
    {
        $organizations = Organization::latest()->get();
        $organizations->map(function ($organization) {
            $organization->photo = env('APP_URL') . Storage::url('organizations/' . $organization->photo);
            return $organization;
        });
        return response()->json([
            'success' => true,
            'message' => 'List Data Organization',
            'data'    => $organizations
        ], 200);
    }

    public function show($id)
    {
        $organization = Organization::find($id);

        $organization->photo = env('APP_URL') . Storage::url('organizations/' . $organization->photo);

        if (!$organization) {
            return response()->json([
                'success' => false,
                'message' => 'Organization tidak ditemukan',
                'data'    => null
            ], 404);
        }

        return response()->json([
            'success' => true,
            'message' => 'Detail Data Organization',
            'data'    => $organization
        ], 200);
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'number_phone' => 'required|max:255',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'email' => 'required|max:255',
            'description' => 'required|max:255',
            'instagram' => 'required|max:255',
        ]);

        $organization = new Organization();

        $organization->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            Storage::putFileAs('public/organizations', $photo, $filename);
            $organization->photo = $filename;
        }

        $organization->save();

        return response()->json([
            'success' => true,
            'message' => 'Organization berhasil ditambahkan',
            'data'    => $organization
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $organization = Organization::find($id);

        if (!$organization) {
            return response()->json([
                'success' => false,
                'message' => 'Organization tidak ditemukan',
                'data'    => null
            ], 404);
        }

        $validateData = $request->validate([
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'number_phone' => 'required|max:255',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'email' => 'required|max:255',
            'description' => 'required|max:255',
            'instagram' => 'required|max:255',
        ]);

        $organization->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            Storage::putFileAs('public/organizations', $photo, $filename);
            Storage::delete('public/organizations/' . $organization->photo);
            $organization->photo = $filename;
        }

        $organization->save();

        return response()->json([
            'success' => true,
            'message' => 'Organization berhasil diupdate',
            'data'    => $organization
        ], 200);
    }

    public function destroy($id)
    {
        $organization = Organization::find($id);

        if (!$organization) {
            return response()->json([
                'success' => false,
                'message' => 'Organization tidak ditemukan',
                'data'    => null
            ], 404);
        }

        $organization->delete();

        return response()->json([
            'success' => true,
            'message' => 'Organization berhasil dihapus',
            'data'    => $organization
        ], 200);
    }
}
