<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index()
    {
        $users = User::latest()->get();
        $users->map(function ($user) {
            $user->photo = env('APP_URL') . Storage::url('users/' . $user->photo);
            return $user;
        });
        return response()->json([
            'success' => true,
            'message' => 'List Data User',
            'data'    => $users
        ], 200);
    }

    public function show($id)
    {
        $user = User::find($id);

        $user->photo = env('APP_URL') . Storage::url('users/' . $user->photo);

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User tidak ditemukan',
                'data'    => null
            ], 404);
        }

        return response()->json([
            'success' => true,
            'message' => 'Detail Data User',
            'data'    => $user
        ], 200);
    }
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'Data User dengan id ' . $id . ' tidak ditemukan',
                'data'    => null
            ], 404);
        }

        $validateData = $request->validate([
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'price' => 'required|max:255',
            'rating' => 'required|max:255',
            'is_recommended' => 'required|max:255|default:0',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'type' => 'required|max:255',
            'number_phone' => 'required|max:255',
        ]);

        $user->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            Storage::putFileAs('public/users', $photo, $filename);
            Storage::delete('public/users/' . $user->photo);
            $user->photo = $filename;
        }

        $user->save();

        return response()->json([
            'success' => true,
            'message' => 'Data User berhasil diupdate',
            'data'    => $user
        ], 200);
    }
}
