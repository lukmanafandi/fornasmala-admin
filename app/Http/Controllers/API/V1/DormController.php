<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Dorm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DormController extends Controller
{
    public function index()
    {
        $dorms = Dorm::latest()->get();
        $dorms->map(function ($dorm) {
            $dorm->photo = env('APP_URL') . Storage::url('dorms/' . $dorm->photo);
            if ($dorm->type == 'male') {
                $dorm->type = 'Laki-Laki';
            } else if ($dorm->type == 'female') {
                $dorm->type = 'Perempuan';
            } else {
                $dorm->type = 'Campur';
            }
            return $dorm;
        });
        return response()->json([
            'success' => true,
            'message' => 'List Data Dorm',
            'data'    => $dorms
        ], 200);
    }

    public function show($id)
    {
        $dorm = Dorm::find($id);

        $dorm->photo = env('APP_URL') . Storage::url('dorms/' . $dorm->photo);

        if (!$dorm) {
            return response()->json([
                'success' => false,
                'message' => 'Dorm tidak ditemukan',
                'data'    => null
            ], 404);
        }

        return response()->json([
            'success' => true,
            'message' => 'Detail Data Dorm',
            'data'    => $dorm
        ], 200);
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'price' => 'required|max:255',
            'rating' => 'required|max:255',
            'is_recommended' => 'required|max:255|default:0',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'type' => 'required|max:255',
            'number_phone' => 'required|max:255',
        ]);

        $dorm = new Dorm();

        $dorm->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            Storage::putFileAs('public/dorms', $photo, $filename);
            $dorm->photo = $filename;
        }

        $dorm->save();

        return response()->json([
            'success' => true,
            'message' => 'Data Dorm berhasil disimpan',
            'data'    => $dorm
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $dorm = Dorm::find($id);

        if (!$dorm) {
            return response()->json([
                'success' => false,
                'message' => 'Data Dorm dengan id ' . $id . ' tidak ditemukan',
                'data'    => null
            ], 404);
        }

        $validateData = $request->validate([
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'price' => 'required|max:255',
            'rating' => 'required|max:255',
            'is_recommended' => 'required|max:255|default:0',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'type' => 'required|max:255',
            'number_phone' => 'required|max:255',
        ]);

        $dorm->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            Storage::putFileAs('public/dorms', $photo, $filename);
            Storage::delete('public/dorms/' . $dorm->photo);
            $dorm->photo = $filename;
        }

        $dorm->save();

        return response()->json([
            'success' => true,
            'message' => 'Data Dorm berhasil diupdate',
            'data'    => $dorm
        ], 200);
    }

    public function destroy($id)
    {
        $dorm = Dorm::find($id);

        if (!$dorm) {
            return response()->json([
                'success' => false,
                'message' => 'Data Dorm dengan id ' . $id . ' tidak ditemukan',
                'data'    => null
            ], 404);
        }

        Storage::delete('public/dorms/' . $dorm->photo);

        $dorm->delete();

        return response()->json([
            'success' => true,
            'message' => 'Data Dorm berhasil dihapus',
            'data'    => $dorm
        ], 200);
    }
}
