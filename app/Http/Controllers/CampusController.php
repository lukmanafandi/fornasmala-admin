<?php

namespace App\Http\Controllers;

use App\Models\Campus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CampusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $campuses = Campus::latest();

        if ($request->has('search') && $request->search != '') {
            $campuses = $campuses->where('name', 'like', '%' . $request->search . '%')
                ->orWhere('email', 'like', '%' . $request->search . '%')
                ->orWhere('number_phone', 'like', '%' . $request->search . '%')
                ->orWhere('address', 'like', '%' . $request->search . '%');
        }

        $campuses = $campuses->paginate(10);

        return view('campuses.index', ['campuses' => $campuses, 'type_menu' => 'campuses']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('campuses.create', ['type_menu' => 'campuses']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'photo' => 'required|file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'number_phone' => 'required|max:255',
            'address' => 'required|max:255',
            'instagram' => 'required|max:255',
            'description' => 'required|max:255',
            'website' => 'required|max:255',
        ]);

        $campus = new Campus();

        $campus->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/campuses/photos', $photo, $fileName);

            $campus->photo = 'photos/' . $fileName;
        }

        $campus->save();

        return redirect()->route('campuses.index')->with('success', 'Campus created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Campus  $campus
     * @return \Illuminate\Http\Response
     */
    public function show(Campus $campus)
    {
        return view('campuses.show', ['campus' => $campus, 'type_menu' => 'campuses']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Campus  $campus
     * @return \Illuminate\Http\Response
     */
    public function edit(Campus $campus)
    {
        return view('campuses.edit', ['campus' => $campus, 'type_menu' => 'campuses']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Campus  $campus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Campus $campus)
    {
        $validateData = $request->validate([
            'name' => 'max:255',
            'email' => 'max:255',
            'photo' => 'file|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'number_phone' => 'max:255',
            'address' => 'max:255',
            'instagram' => 'max:255',
            'description' => 'max:255',
            'website' => 'max:255',
        ]);

        $campus->fill($validateData);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '_' . $photo->getClientOriginalName();
            Storage::putFileAs('public/campuses/photos', $photo, $fileName);

            $campus->photo = 'photos/' . $fileName;
        }

        $campus->save();

        return redirect()->route('campuses.index')->with('success', 'Campus updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Campus  $campus
     * @return \Illuminate\Http\Response
     */
    public function destroy(Campus $campus)
    {
        $campus->delete();

        return redirect()->route('campuses.index')->with('success', 'Campus deleted successfully.');
    }
}
